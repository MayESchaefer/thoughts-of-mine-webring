const aboutURL = "https://webring.thoughtsofmine.ca/";
const webringURL = "https://webring.thoughtsofmine.ca/";
const localBaseURL = window.location.origin ? window.location.origin + '/' : window.location.protocol + '/' + window.location.host + '/';
fetch(webringURL + "ring.json").then(response => response.json().then(items => {
	let myIndex = 0;
	for (let i = 0; i < items.length; i++) {
		if (items[i].url === localBaseURL) {
			myIndex = i;
			break;
		}
	}
	let prev = myIndex - 1;
	let next = myIndex + 1;
	if (myIndex === 0) {
		prev = items.length - 1;
	}
	else if (myIndex === items.length - 1) {
		next = 0;
	}
	let rand = myIndex;
	while (rand === myIndex) {
		rand = Math.floor(Math.random() * items.length);
	}
	let content = "";
	content += `<td class="webring-left"><a id="webring-prev" href="${items[prev].url}">previous</a></td>`;
	content += `<td class="webring-center"><div id="webring-description">This site is part of the Thoughts of Mine webring</div>`;
	content += `<a id="webring-rand" href="${items[rand].url}">random</a>`;
	content += `&nbsp;`;
	content += `<a id="webring-about" href="${aboutURL}">about</a>`;
	content += `</td><td class="webring-right"><a id="webring-next" href="${items[next].url}">next</a></td>`;

	document.getElementById("webring-circle").innerHTML = content;
}));
