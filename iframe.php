<?php
$baseURL = "https://" . $_SERVER['SERVER_NAME'];
$items = json_decode(file_get_contents($baseURL . "/ring.json"), true);

echo "<!DOCTYPE html><html><head>";

if (isset($_GET["css"]) && filter_var(urldecode($_GET["css"]), FILTER_VALIDATE_URL)) {
	echo '<link rel="stylesheet" href="' . urldecode($_GET["css"]) . '">';
}
else {
	echo '<link rel="stylesheet" href="' . $baseURL . '/iframe_style.css">';
}

echo '</head><body><div class="webring-items">';

foreach ($items as $item) {
	if (!(isset($_GET["exclude"]) && urldecode($_GET["exclude"]) === $item["url"])) {
		$text = $item["text"];
		$url = $item["url"];
		if (isset($item["image"])) {
			$image = $item["image"];
			echo '<a class="webring-item-image" href="'.$url.'" target="_top"><img src="'.$image.'" alt='.$text.'"/></a>';
		}
		else {
			echo '<a class="webring-item-text" href="'.$url.'" target="_top">'.$text.'</a>';
		}
	}
}

echo '</div></body></html>';
?>