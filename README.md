# ThoughtsOfMine Webring

This is a little webring I decided to throw together for my site! If you know me, you should make a Merge Request to add yourself!

Use the examples in the `example` folder to add it to your own site - remember to replace `https://example.net/` with your own site's URL!

The iframe examples accept two GET parameters:

- The first is demoed in the example: `exclude` is the root URL of the current site, to be excluded from the list in the linkroll example, or to provide an index for the webring example
- The second is not demoed: `css` is the URL for a CSS stylesheet, which replaces the default stylesheet if provided.
